import {json, type RequestHandler, text} from "@sveltejs/kit";

export const GET: RequestHandler = async ({params,fetch}) => {
    const {id} = params;
    const response = await fetch(`https://cyoa-ai.alexk8s.com/v1/adventure/${id}.txt`);
    if (response.ok) {
        const body = await response.text();
        const a=  {
            headers: {
                'content-type': 'text/plain;charset=UTF-8',
            },
            status: 200,
        };
        return text(body,a)
    }
    return json({error: "Adventure not found"});
}