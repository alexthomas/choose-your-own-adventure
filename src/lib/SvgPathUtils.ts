export type Rectangle = {
    x: number;
    y: number;
    width: number;
    height: number;
    stroke?: string;
    'stroke-width'?: number;
    fill?: string;
    id: string;
}
export type Path = {
    d: string;
    stroke: string;
    'stroke-width': number;
    fill: string;
    id: string;
}

export type Circle = {
    x: number,
    y: number,
    r: number,
    stroke: string,
    'stroke-width': number,
    fill: string,
    id: string
}
const curveRadius = 4;

export function getPath(rect1: Rectangle, rect2: Rectangle, verticalOffset = 0, horizontalOffset = 0, terminalHorizontalOffset = 0): Path {
    const x1 = rect1.x + rect1.width;
    const y1 = rect1.y + rect1.height / 2 + verticalOffset;
    const x2 = rect2.x;
    const y2 = rect2.y + rect2.height / 2 + terminalHorizontalOffset;
    const d = getPathD(x1, y1, x2, y2, curveRadius, horizontalOffset);
    return {d, stroke: 'black', 'stroke-width': 3, fill: 'none', id: `${rect1.id}-${rect2.id}`}
}

function getPathD(x1: number, y1: number, x2: number, y2: number, curveRadius: number, horizontalOffset = 0) {

    if (Math.abs(y1 - y2) < curveRadius * 2) {
        return `
        M ${x1} ${y1} 
        L ${x2} ${y1}
        `
    }
    let x3 = x2 - x1 - curveRadius * 3;
    let y3 = y2 - y1 - curveRadius * 2;
    const verticalDirection = y3 / Math.abs(y3);
    const horizontalDirection = x3 / Math.abs(x3);
    if (verticalDirection === -1)
        y3 = y3 + curveRadius * 4;
    if (horizontalDirection === -1)
        x3 = x3 + curveRadius * 4;
    return `M ${x1} ${y1} h ${10 + horizontalOffset}
        c 0,0 ${curveRadius},0 ${curveRadius},${curveRadius * verticalDirection}
        v ${y3}
        c 0,0 0,${curveRadius * verticalDirection} ${curveRadius * horizontalDirection},${curveRadius * verticalDirection}
        h ${x3}`;
}

export function getCirclePath(circle1: Circle, circle2: Circle, curveRadius: number = 4): Path {
    const x1 = circle1.x ;
    const y1 = circle1.y ;
    const x2 = circle2.x-circle2.r ;
    const y2 = circle2.y ;
    const d = getPathD(x1, y1, x2, y2, curveRadius, circle1.r/2);
    return {d, stroke: 'black', 'stroke-width': 3, fill: 'none', id: `${circle1.id}-${circle2.id}`}

}

export function getPadding(rectangle: Rectangle, offset = 0): Path {
    const curveRadius = 5;
    const d = `
        M ${rectangle.x - offset} ${rectangle.y}
        v ${rectangle.height - (curveRadius - offset)}
        c 0,0 0,${curveRadius} ${curveRadius},${curveRadius}
        h ${rectangle.width + (offset - curveRadius) * 2}
        c 0,0 ${curveRadius},0 ${curveRadius},${-curveRadius}
        v ${-rectangle.height - (offset - curveRadius) * 2}
        c 0,0 0,${-curveRadius} ${-curveRadius},${-curveRadius}
        h ${-rectangle.width - (offset - curveRadius) * 2}
        c 0,0 ${-curveRadius},0 ${-curveRadius},${curveRadius}
        v ${offset}
        `;
    return {d, stroke: '#bbb', 'stroke-width': 2, fill: 'white', id: `${rectangle.id}-padding`}
}

