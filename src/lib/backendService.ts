import type {Adventure, AdventureNode, RemainingCredits} from "$lib/types";
import {getToken} from "$lib/auth";

const BACKEND_URL = "https://cyoa-ai.alexk8s.com"
export async function getAuthHeaders(): Promise<{[key:string]:string}> {
    const token = await getToken();
    return {
        "Authorization": `Bearer ${token}`,
        "Content-Type": "application/json"
    };
}

export async function getAdventures(): Promise<Adventure[]> {
    const response = await fetch(`${BACKEND_URL}/v1/adventure`)
    return await response.json()
}

export async function getAdventure(id: string): Promise<Adventure> {
    const response = await fetch(`${BACKEND_URL}/v1/adventure/${id}`)
    return await response.json()
}

export async function getAdventureStart(id: string): Promise<AdventureNode> {
    const response = await fetch(`${BACKEND_URL}/v1/adventure-node/adventure/${id}/start`)
    return await response.json()
}

export async function getAdventureNodes(adventureId: string): Promise<AdventureNode[]> {
    const response = await fetch(`${BACKEND_URL}/v1/adventure-node/adventure/${adventureId}`)
    return await response.json()
}

export async function getNodeChoices(id: string): Promise<AdventureNode[]> {
    const response = await fetch(`${BACKEND_URL}/v1/adventure-node/${id}/choices`)
    return await response.json()
}

export async function generateChoice(id: string, guidance: string | null): Promise<AdventureNode> {
    const headers = await getAuthHeaders();
    const response = await fetch(`${BACKEND_URL}/v1/adventure-node/${id}/generate`, {
        method: "POST",
        headers,
        body: JSON.stringify({guidance})
    });
    return await response.json()
}

export async function makeChoice(id: string): Promise<void> {
    const headers = await getAuthHeaders();
    const response = await fetch(`${BACKEND_URL}/v1/adventure-node/${id}/choice`, {
        method: "POST",
        headers
    });
}

export async function generateAdventure(prompt: string): Promise<Adventure> {
    const headers = await getAuthHeaders();
    const response = await fetch(`${BACKEND_URL}/v1/adventure`, {
        method: "POST",
        headers,
        body: JSON.stringify({prompt})
    });
    return await response.json()
}

export async function createNode(adventureId: string, parentId: string, text: string): Promise<AdventureNode> {
    const headers = await getAuthHeaders();
    const response = await fetch(`${BACKEND_URL}/v1/adventure-node`, {
        method: "POST",
        headers,
        body: JSON.stringify({adventureId, parentId, title: text})
    });
    return await response.json()
}

export async function getAdventureNode(id: string): Promise<AdventureNode> {
    const response = await fetch(`${BACKEND_URL}/v1/adventure-node/${id}`)
    return await response.json()
}

export async function getChain(nodeId: string): Promise<string[]> {
    const response = await fetch(`${BACKEND_URL}/v1/adventure-node/${nodeId}/chain`)
    return await response.json()
}

export async function regenerateNode(id: string): Promise<AdventureNode> {
    const headers = await getAuthHeaders();
    const response = await fetch(`${BACKEND_URL}/v1/adventure-node/${id}/regenerate`, {
        method: "POST",
        headers
    });
    return await response.json()
}

export async function getRemainingCredits():Promise<RemainingCredits>{
    const headers = await getAuthHeaders();
    const response = await fetch(`${BACKEND_URL}/v1/credits`, {
        headers
    });
    return await response.json();
}

export async function getTtsUrl(nodeId:string):Promise<{url:string}> {
    const headers = await getAuthHeaders();
    delete headers["Content-Type"];
    const response = await fetch(`${BACKEND_URL}/v1/adventure-node/${nodeId}/tts`, {
        headers
    });
    return await response.json();
}