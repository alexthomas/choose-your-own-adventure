import {app} from './firebase'
import {
    getAuth,
    createUserWithEmailAndPassword,
    signInWithEmailAndPassword,
    signOut as firebaseSignOut,
    type User,
    type UserCredential,
    signInWithPopup,
    GithubAuthProvider,
    GoogleAuthProvider
} from "firebase/auth";
import {writable} from "svelte/store";
import {datadogRum} from "@datadog/browser-rum";

export const user = writable<User | null>();

const auth = getAuth();
auth.onAuthStateChanged(u => user.set(u))
user.subscribe(u => {
    const userData = u?.providerData || [];
    const id = userData.map(d => d.uid).filter(d => !!d)[0];
    const email = userData.map(d => d.email).filter(d => !!d)[0]!;
    const name = userData.map(d => d.displayName).filter(d => !!d)[0]!;
    const emailVerified = u?.emailVerified;
    const isAnonymous = u?.isAnonymous;
    datadogRum.setUser({id, email, name, userData, emailVerified, isAnonymous});
})

export async function authenticate(email: string, password: string): Promise<UserCredential> {
    return await signInWithEmailAndPassword(auth, email, password);
}

export async function githubSignIn(): Promise<UserCredential> {
    const provider = new GithubAuthProvider();
    return await signInWithPopup(auth, provider);
}

export async function googleSignIn(): Promise<UserCredential> {
    const provider = new GoogleAuthProvider();
    return await signInWithPopup(auth, provider);
}

export async function createUser(email: string, password: string): Promise<UserCredential> {
    try {
        return await createUserWithEmailAndPassword(auth, email, password);
    } catch (e) {
        // @ts-ignore
        const {code, message} = e;
        throw new Error(message);
    }
}

export function signOut(): Promise<void> {
    return firebaseSignOut(auth);
}

export async function getToken(): Promise<string> {
    if (!auth.currentUser) {
        throw new Error("Not authenticated");
    }
    return await auth.currentUser.getIdToken()
}

export function isAuthenticated(): boolean {
    return !!auth.currentUser;
}

