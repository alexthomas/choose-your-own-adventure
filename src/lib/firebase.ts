// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyBZOdluM1eqz-CPGc4raZd5JoGu_ayzYvk",
    authDomain: "cyoa-ai.firebaseapp.com",
    projectId: "cyoa-ai",
    storageBucket: "cyoa-ai.appspot.com",
    messagingSenderId: "774235168250",
    appId: "1:774235168250:web:93a08d8c69cb1b935d9843"
};

// Initialize Firebase
export const app = initializeApp(firebaseConfig);