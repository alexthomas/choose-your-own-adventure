export type Adventure = {
    id: string,
    title: string,
    tags: string[],
}

export type AdventureNode = {
    id: string,
    adventureId: string,
    parentId: string,
    generated: boolean,
    content: string | null,
    title: string,
    depth: number,
    summary: string,
    userCreated: boolean,
    ttsUrl: string | null,
}

export type TextSection = {
    text: string,
    bold: boolean,
    italic: boolean,
}

export type RemainingCredits = {
    remaining: number,
    total: number,
    resetsAt: string,
    resetInSeconds: number,
}