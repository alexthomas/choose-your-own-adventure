import type {TextSection} from "$lib/types";

export function parseSections(text:string):TextSection[] {
    const sections:TextSection[] = [];
    let bold = false;
    let italic = false;
    let current = "";
    for (let i = 0; i < text.length; i++) {
        if (text[i] === "*") {
            if (current) {
                sections.push({text: current, bold, italic});
                current = "";
            }
            bold = !bold;
        } else if (text[i] === "_") {
            if (current) {
                sections.push({text: current, bold, italic});
                current = "";
            }
            italic = !italic;
        } else {
            current += text[i];
        }
    }
    if (current) {
        sections.push({text: current, bold, italic});
    }
    return sections;
}